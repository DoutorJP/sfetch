#include <iostream>
#include <sys/utsname.h>
#include <fmt/core.h>
#include <fmt/color.h>
#include <string>
#include <fstream>
#include <stdio.h>
#include <unistd.h>

int main(){
  struct utsname up;
  uname(&up);
  std::ifstream info;
  info.open("/etc/os-release");
  std::string name, sh = getenv("SHELL");
  getline(info, name);
  name = name.substr(5, name.size());
  
  fmt::print(fmt::emphasis::bold, "User: {}\nOS: {}\nKernel: {}\nShell: {}\n", getenv("USER"), name, up.release, sh);
  
  return 0;
}
